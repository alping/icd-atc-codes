import yaml, requests, io, re
import pandas as pd
from zipfile import ZipFile
from typing import Dict
from pathlib import Path


def make_dataset(zipfile: ZipFile) -> pd.DataFrame:
    """ Processes the file contents and creates a dataframe """

    # Decode the file to text
    file_contents = zipfile.read().decode("utf-8").replace("\ufeff", "")

    # Separate code and description
    data = [
        t.replace("*", "").replace("†", "").split(maxsplit=1)
        for t in file_contents.split("\r\n")
        if t
    ]

    # Create DataFrame
    df = pd.DataFrame(data, columns=["code", "description"])

    # Return the DataFrame
    return df


if __name__ == "__main__":
    # Data paths
    base_dir = Path(__file__).resolve().parents[1]
    data_dir = base_dir.joinpath("icd")

    # Load YAML source data
    with open(base_dir.joinpath("icd.yaml"), "r") as f:
        icd_src = list(yaml.safe_load_all(f))

    # Export the cleaned ICD data
    for src in icd_src:
        # Download archive from URL
        r = requests.get(src["url"])

        # If download OK
        if r.ok:
            # Extract file
            with ZipFile(io.BytesIO(r.content)) as z:
                with z.open(src["file"]) as f:
                    icd_dataset = make_dataset(f)
        else:
            raise ValueError("File not downloaded from URL")

        # Path to save CSV based on key and year
        path = data_dir.joinpath(src["key"]).joinpath(str(src["year"]))

        # If directory doesn't exist, create it
        if not path.is_dir():
            path.mkdir(parents=True)

        # Export dataset to csv
        icd_dataset.to_csv(
            path.joinpath(f"icd-{src['key']}-{src['year']}.csv"), index=False
        )

#   codes: digit4.txt
#   categories: digit3.txt
#   subchapters: block.txt
#   chapters: chapter.txt
