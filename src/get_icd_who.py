import requests
import pandas as pd
from itertools import chain

api_root = "https://icd.who.int/browse10/2016/en/JsonGetRootConcepts"
api_children = "https://icd.who.int/browse10/2016/en/JsonGetChildrenConcepts"
api_params = dict(useHtml=False)


# from numpy import base_repr
# def icd_range(start, stop):
#     return [
#         base_repr(i, 36)
#         for i in range(int(start, 36), int(stop, 36) + 1)
#         if base_repr(i, 36)[1:].isnumeric()
#     ]


def request_json(url, params):
    """Returns the requested json or None if the request failed"""
    response = requests.get(url, params=params)
    if response.ok:
        return response.json()
    else:
        return None


def get_root():
    """Retrieves the root concepts"""
    return request_json(api_root, api_params)


def get_children(concept):
    """Retrieves the children of the given concept"""
    return request_json(api_children, {"ConceptId": concept, **api_params})


def get_keys(data):
    """Extracts the keys for the next level of walking through the API"""
    return [d["ID"] for d in data if not d["isLeaf"]]


################################################################################
# Walk the codes
################################################################################
# Initialize result data list and keys
data = [get_root()]
keys = get_keys(data[0])

# Walk through keys until no keys left
while keys:
    # Get the current level
    level = len(data)

    # Print the progress
    print(f"Level={level}, keys={len(keys)}")

    # Append the new level data
    data.append(list(chain(*[get_children(k) for k in keys])))

    # Get new keys
    keys = get_keys(data[level])

################################################################################
# Create and export dataset
################################################################################
df = (
    pd.concat(pd.DataFrame(d).assign(level=i) for i, d in enumerate(data))
    .rename(columns={"ID": "code", "label": "description", "isLeaf": "end_node"})
    .astype({"end_node": int})
    .assign(
        description=lambda x: x["description"].str.split(n=1, expand=True)[1],
        chapter=lambda x: (x["level"] == 0).astype(int),
        block=lambda x: x["code"].str.contains("-").astype(int),
        length=lambda x: x["code"].str.len()
    )
    .sort_values(
        ["chapter", "block", "length", "code"], ascending=[False, False, True, True]
    )
    .drop(columns="length")
    .set_index("code")
    .loc[:, ["description", "level", "chapter", "block", "end_node"]]
    .to_csv("icd/who/who-all-icd.csv")
)
