import yaml, requests, io, xmltodict
import pandas as pd
from zipfile import ZipFile
from typing import Dict
from pathlib import Path


def make_atc_dataset(file: ZipFile) -> pd.DataFrame:
    """ Processes the file contents and creates a dataframe """

    atc_dict = xmltodict.parse(file)

    schema = atc_dict['xs:schema']['xs:simpleType']['xs:restriction']['xs:enumeration']
    atc = {}

    for e in schema:
        atc[e['@value']] = {
            e['xs:annotation']['xs:documentation'][0]['@xml:lang']:
                e['xs:annotation']['xs:documentation'][0]['#text'],
            e['xs:annotation']['xs:documentation'][1]['@xml:lang']:
                e['xs:annotation']['xs:documentation'][1]['#text']}

        if 'xs:appinfo' in e['xs:annotation']:
            atc[e['@value']]['not_used'] = e['xs:annotation']['xs:appinfo'][-10:]

    df = pd.DataFrame(atc).T.reset_index()

    df = df.rename(columns=dict(
        index='code',
        sv='se_description',
        en='en_description'))

    # Fix order
    df = df[['code', 'se_description', 'en_description', 'not_used']]

    # Return the DataFrame
    return df


def get_atc(url: str, files: Dict[str, str]) -> Dict[str, pd.DataFrame]:
    # Download file
    r = requests.get(url)

    # Result dictionary
    atc = dict()

    # If download ok
    if r.ok:
        # Extract files
        with ZipFile(io.BytesIO(r.content)) as z:
            # Go through all files and make datasets
            for name, file in files.items():
                with z.open(file) as f:
                    atc[name] = make_atc_dataset(f)

    return atc


if __name__ == '__main__':
    # Data paths
    base_dir = Path(__file__).resolve().parents[1]
    data_dir = base_dir.joinpath('atc')

    # Load YAML source data
    with open(base_dir.joinpath('atc.yaml'), 'r') as f:
        atc_src = list(yaml.safe_load_all(f))

    # Export the cleaned ICD data
    for src in atc_src:
        for name, ds in get_atc(src['url'], src['files']).items():
            # Path to save CSV based on key and year
            path = data_dir.joinpath(src['key'])
            # If directory doesn't exist, create it
            if not path.is_dir(): path.mkdir(parents=True)
            # Export dataset to csv
            ds.to_csv(path.joinpath(f'{name}.csv'), index=False)
